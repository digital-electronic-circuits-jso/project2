**** Ehsan Jahangirzadeh **** 
**** 810194554 ****
**** CA2 ****

**** Load libraries ****
.inc '45nm.pm'
**** Params ****
.param slp=0.1p
.param Out_T_DLY=12.95
.param PRD=13

.global Vdd
.temp 25

.param	lmin=45n
+Vdd=1V

**** Source Voltage ****
VSupply		    vss	    gnd		DC		   Vdd
VA				a		gnd		DC	        0	
VB				b		gnd		DC 		    0
VC				c		gnd	   Pulse        0   	Vdd		0	1p	1p	50p  100p

**** NOR ****
M6				1		c		vss		vss		pmos		l='lmin'		w='12*lmin'
M5				2		b		1		1		pmos		l='lmin'		w='12*lmin'
M4				out		a		2		2		pmos		l='lmin'		w='12*lmin'	

M1 				out		a		0		0		nmos		l='lmin'		w='2*lmin'
M2 				out		b		0		0		nmos		l='lmin'		w='2*lmin'
M3 				out		c		0		0		nmos		l='lmin'		w='2*lmin'

**** Analysis ****
.tran 	1p 500p	

**** Meas ****
.meas 	tran	AVGpower avg	Power	from=0		to=500p

.END